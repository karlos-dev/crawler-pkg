import scrapy
import sys, getopt

from scrapy.crawler import CrawlerProcess
from scrapy.crawler import Crawler
from scrapy.utils.project import get_project_settings
from www_spider import WwwSpider

ua_set=['Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36']
settings = get_project_settings()
settings.set('USER_AGENT',ua_set[0])

def main(argv):

	load_js=False
	keys=[]

	try:
		opts, args = getopt.getopt(argv,"u:o:js",["ifile=","ofile="])
		if not opts:
			raise getopt.GetoptError('')
			#print "No opts\n" 
		else:
			print opts

		if len(args) > 0:
			print args
		else: 
			print("No args") 
			#raise getopt.GetoptError('')
		for opt, arg in opts:
			if opt in ('-u'):
				starturl = arg
			if arg in ('-js'):
				load_js=True

		process = CrawlerProcess( settings )
		process.crawl(WwwSpider, url=starturl, keys=keys, js=load_js, run_id='7ed5aa0ddca681fc605d7fff6f4d68bd')
		process.start()

	except getopt.GetoptError:
	    print '\nUsage:\n\trun.py -u <url>\n'
	    sys.exit(2)

if __name__ == "__main__":
   main(sys.argv[1:])