# -*- coding: utf-8 -*-
import scrapy
import logging, os, hashlib, random, time, requests, itertools
import urlparse, json, sys, socket

from datetime import datetime
from urlparse import urlparse
from requests.auth import HTTPBasicAuth
from scrapy.exceptions import CloseSpider
from scrapy.linkextractors import LinkExtractor
from scrapy.crawler import CrawlerProcess
from scrapy.spiders import CrawlSpider, Rule

from scrapy.utils.log import configure_logging

"""WwwSpider
  v0.1
  This crawler was designed to collect useful links from useful websites
  'Useful' was determined using simple maps of keywords in links and page texts.

  v0.2
  Now it has been modified to navigate outside domains to the root url
  to generate a report of visited domains which are geolocated,
  linked each other in a tree-like structure,
  to be easily drawn using geolocations in google maps.

  Limitations:
    stores domains in a file
    geolocation is a secuential process
""" 

BASE_DIR=path=os.path.dirname(os.path.abspath(__file__))
DENY = { 
  'domains':['cia.gov'],
  'extensions':[ 
    '.php', 
    '.css', 
    '.js', 
    '.tld', 
    '.dtd', 
    '.rss', 
    '.xml',
    '.pdf'
  ],
  'tls':[
    'cn',
    'jp',
    'ru',
    'com.cn'
  ]
}

configure_logging(install_root_handler=False)
logging.basicConfig(
    filename='craw.log',
    format='%(levelname)s: %(message)s',
    level=logging.INFO
)

### Utility Methods

def md5(string):
    m = hashlib.md5()
    m.update(string.encode('utf-8'))
    return m.hexdigest()


def isAscii(s):
  try:
    s.decode('ascii')
  except UnicodeDecodeError :
    return False
  except UnicodeEncodeError:
    return False
  else:
    return True


class Save(object):
  """Save
  
    Dumps a full dict() as a json object into a json file 
    which can be easily loaded using: json.loads(file)
  
  Variables:
    filed {int} -- This is a file descriptor
  """
  filed = None
  def __init__(self,obj,name):
    self.filed = open( str(name)+'.json', 'w')
    line = json.dumps(obj)
    self.filed.write(line)
    self.filed.close()


class Append(object):

  filed = None
  reportdir = os.path.dirname(os.path.realpath(__file__)) + '/../reports/'

  def __init__(self,arr, name):
    dir_path = os.path.dirname(os.path.realpath(__file__))

    self.filed = open( self.reportdir + str(name)+'.txt', 'a+')
    #line = json.dumps(dict(object)) + "\n"
    for line in arr:
      try:
        clean = line.encode('utf-8').translate(None, ''.join(['?','¿','.','!',':',';','(',')','"',"'",'[',']','{','}']) )
        self.filed.write(clean + '\n')
      except:
        pass
        
    self.filed.close()


def saneUrl(url):
  """saneUrl checks for a useful url
  
  This could be extended using options.py
  
  Arguments:
    url {str} -- The url to sanitize
  
  Returns:
    bool -- If str is sane or not
  """
  nosane=['porn','xxx','mature','violent','rotten','tube','chick','baby','sex','blowjob','babes','fucking']
  for k in nosane:
    if k in url:
      return False
  return True


def cleanText(text):
    """cleanText Checks some conditions in a text word to determine if it is valid

      Returns:
        bool
    """
    chars = set('0123456789$€|')
    text = text.replace('&nbsp;','')
    text = text.replace('...','')

    return isAscii(text) and len(text)>1 and not any((c in chars) for c in text) \
      and not '\n' in text and not '\t' in text and not 'function(' in text \
      and text != '' and text != ' ' and text != ','

### Spider class

class WwwSpider(scrapy.Spider):
    name = "www"

    allowed_domains=[]
    start_urls=[]    
    custom_settings={ 'LOG_ENABLED': True, 'DOWNLOAD_DELAY' : 0.1 }
    
    debug=True
    keywords=[]
    out_keywords={}

    #algo Settings
    start=time.time()
    baseDomain=''
    end=0
    logger=logging.getLogger(name)
    visited=[]
    visitedcount=0
    domains=[]
    domainscount=0
    outsideBaseDomain=False
    runid=0
    trycount=0
    level=0
    links=[]
    reparse_instances=0
    MAX_LINKS=100
    MAX_LEVELS=10

    index=[]
    ff=None
    deny_tls=[]
    link_max_size=286
    run_id=None
    user_id=None

    le = LinkExtractor(deny_domains=DENY['domains'], deny_extensions=DENY['extensions'])


    def __init__(self, *args, **kwargs):
      super(WwwSpider, self).__init__(*args, **kwargs)

      url= kwargs.get('url')
      mode= kwargs.get('mode')
      self.run_id= kwargs.get('run_id')

      self.outsideBaseDomain = True #kwargs.get('outside')
      self.runhash = kwargs.get('runhash')

      workerdata="%s/workerdata"%BASE_DIR
      self.ff=open( '%s/visits_%s.csv'%(workerdata,self.run_id), 'w')
      self.deny_tls=DENY['tls']

      if not url :
        raise CloseSpider('Not start url set')
      else:
        o = urlparse(url)
        d = o.netloc.replace("www.", "")
        arr=d.split('.')[len(d.split('.'))-2:len(d.split('.')) ]
        domain='.'.join(arr)
        self.start_urls += [url]
        self.baseDomain = domain
        self.level=1
        try:
          ip=socket.gethostbyname(domain)
          print(ip)
          line = ip+','+domain+','+domain+','+str(self.level)+'\n'
          self.ff.write(line)
        except socket.gaierror as e:
          self.logger.debug(e)

        # Load keyworkds from options
        self.keywords = []

        if not mode :
          self.custom_settings = { 'LOG_ENABLED': True, 'DOWNLOAD_DELAY' : 2.15 }
          self.MAX_LINKS = 100
          self.MAX_LEVELS = 10
        else:
          if mode == 'gentle':
            self.custom_settings = { 'LOG_ENABLED': True, 'DOWNLOAD_DELAY' : 2.8 }
            self.MAX_LINKS = 100
            self.MAX_LEVELS = 5
          else:
            raise Warning('Mode is unconfigured')
            
      self.logger.info("%s crawling urls %s" % (self.name, self.start_urls)) 


    def parse(self, response):
      if self.custom_settings['DOWNLOAD_DELAY'] < 1 :
        raise Warning('Download Delay is under "safe minimum"')

      print(response.headers)
      print(response.meta)

      level=self.level+1
      links=self.le.extract_links(response)
      self.logger.info("Found %d links." % len(links)) 
      page_text = response.xpath('//text()').extract()

      for l in links:
        if self.visit(l.url) and saneUrl(l.url):
          domain, parentdomain = self.extractDomain(l.url, response.url)
          if domain != parentdomain:
            self.geo(domain, parentdomain, level)
            if level < self.MAX_LEVELS :
              request =  scrapy.Request( l.url, callback=self.parse2)
              request.meta['dont_redirect'] = True
              request.meta['level'] = level+1
              request.meta['linked'] = False
              yield request


    def parse2(self, response):
      level = response.meta['level']
      linked = response.meta['linked']
      links = self.le.extract_links(response)
      l=None
      found = False
      keys = []
      for link in links:
        domain, parentdomain = self.extractDomain(link.url, response.url)
        if domain != self.baseDomain and domain not in self.domains:
          if self.visit(link.url) and saneUrl(link.url):
            self.geo(domain, parentdomain, level)

            if level < self.MAX_LEVELS :
              request =  scrapy.Request( link.url, callback=self.parse2)
              request.meta['dont_redirect'] = True
              request.meta['level'] = level+1
              request.meta['linked'] = False
              yield request

        else:
          self.logger.info("NO %s" % link.url) 


    def extractDomain(self, link, parent):
      o = urlparse(link)
      d = o.netloc.replace("www.", "")
      arr=d.split('.')[len(d.split('.'))-2:len(d.split('.')) ]
      domain='.'.join(arr)

      o = urlparse(parent)
      d = o.netloc.replace("www.", "")
      arr=d.split('.')[len(d.split('.'))-2:len(d.split('.')) ]
      parentdomain='.'.join(arr)

      return (domain, parentdomain)


    def geo(self, d, parent, level):
      try:
        ip=socket.gethostbyname(d)
        print("Visited domains %d/%d"%(self.domainscount, self.MAX_LINKS))
        if self.domainscount > self.MAX_LINKS:
          print("Raise CloseSpider")
          raise CloseSpider('maxlinks_exceeded')

        line = ip+','+d+','+parent+','+str(level)+'\n'
        self.ff.write(line)
      except socket.gaierror as e:
        self.logger.debug("%s %s" % (socket.gaierror, e) )


    def inkeys(self, word):
        if '/' in word: # is a link
            wordarr = word.split('/')
            for word in wordarr:
                if word.lower() in self.keywords:
                    self.addkey( word.lower() )
                    #print wordarr
                    return True
        
        elif word.lower() in self.keywords:
            self.addkey( word.lower() )
            return True
        
        return False


    def visit( self, link ):
      if self.domainscount >= self.MAX_LINKS:
        print("Max domains %s reached." % self.MAX_LINKS)
        print("Raise CloseSpider")
        raise CloseSpider('maxlinks_exceeded')

      if link not in self.visited:
        o = urlparse(link)
        d = o.netloc.replace("www.", "")
        arr=d.split('.')[len(d.split('.'))-2:len(d.split('.')) ]
        domain='.'.join(arr)
        tls=domain[domain.rfind('.'):]

        if tls not in self.deny_tls:
          if domain != self.baseDomain and self.outsideBaseDomain:
            self.logger.info('Outside base domain: %s', domain )
            if domain not in self.domains:
              self.domains += [domain]
              self.domainscount += 1
              self.logger.info('domains visited(%s) - %s', self.domainscount,  domain )

          elif domain == self.baseDomain:
            if domain not in self.domains:
              self.domains += [domain]
              self.domainscount += 1
          else:
            self.logger.info('conflict %s --> %s', domain, self.baseDomain )
            self.logger.info('no follow(%s)', link )
            return False
        else:
          self.logger.info('TLS Forbidden: %s', tls)

        self.visited += [link]         
        self.visitedcount += 1
        self.logger.info('links   visited(%s) - %s', self.visitedcount,  link )
        return True

      return False


    def addkey( self, key ):
      if key not in self.out_keywords.keys():
        self.out_keywords[key] = 1
      else:
        value = self.out_keywords[key]
        self.out_keywords[key] = value + 1


    def levelup( self ):
      # for ease
      self.level += 1


    def closed( self, reason ):
      print("Closing spider %s" % reason)

      self.end = time.time()
      self.ff.close()
      self.logger.info("Spider %s visited (%s/%s) links  in %s seconds\n" % (self.name, self.visitedcount, self.trycount, self.end - self.start ))
      
      print("Visited domains:")
      for dom in self.domains:
        print("%s" % (dom))
      
      if len(self.domains) > 3:
        path="%s/geo.py %s"%(BASE_DIR, self.run_id)
        self.logger.info("Geolocating Run results")
        os.system('python '+path)

