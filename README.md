# Scrapy runner
Scrapy Demo Code

### Intro

This source uses scrapy to get all outside domains from a root url.  
Then the crawler visits every domain, saving ip addres and crawler level  
to a csv file in a tree like structure.  
The maximum of domains, for brevity is set to 100. These are distinct domain names visited.  
So:  
`en.wikipedia.org`  
`es.wikipedia.org`  
are the same domain: **wikipedia.org**

The aim of this project is not really to collect information from articles or match text  
against keywords, but to collect and geocode domain names to their physical address (`ip`), and this way,  
obtain geographical information out of the url.  

The processs generates a navigation tree from the start domain and elaborates results in Google Maps API.
Visit the online demo in [this](http://crawler.milnomada.org/) link.

### Install

`virutalenv venv`  
`source venv/bin/activate`  
`pip install -r requirements`  


### Run

`python run.py -u <url>`

### Limitations

 - http://name.co.uk like url's are reduced to co.uk so not working
 - no javascript enabled browsing 
 - Settings for max domains: MAX_LINKS=100

