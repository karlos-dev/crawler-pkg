import sys, os, json, time, datetime, getopt
import requests
from requests.auth import HTTPBasicAuth

"""Geo

  Read ips form csv file, where the ip is in the first position. (ip, domain)
  Writes a csv file structured as: 
  ip, domain, parentdomain, level, lat, lng, countrycode

  Uses the service:
    https://geojs.io/docs/v1/endpoints/

  Author milnomada.org <karlos.alvan@gmail.com>
"""

BASE_DIR=os.path.dirname(os.path.abspath(__file__))
HTTP_HOST= ''
err= '[ERROR]'
info='[INFO]'
application_path='%s/geodata'%BASE_DIR
workerdata="%s/workerdata"%BASE_DIR

if len(sys.argv)>1:
  if len(sys.argv)>3:
    if sys.argv[3] == 'del':
      print("%s %s"%(info,"Delete run."))
      sys.exit(0)
  else:
    try:
      print("%s %s %s"%(info,"Creating run file",sys.argv[1]))
      filed = open('%s/%s.csv' % (application_path, sys.argv[1]), 'w')
    except Exception as e:
      print("%s %s"%(err, "Creating run file"))
      print(e)
      sys.exit(1)
else:
  print("%s %s"%(err, "Rotating file deactivated. New source filename"))
  sys.exit(0)

with open('%s/visits_%s.csv' % (workerdata,sys.argv[1]) ) as f:
  first_line = f.readline()
  while first_line or first_line != '':    
    print("%s %s %s"%(info, "Sending request for", first_line.split(',')[0]))
    url='https://get.geojs.io/v1/ip/geo/%s.json' % first_line.split(',')[0]
    headers={}
    r=requests.get(url)
    try:
      json = r.json()
      line= "%s,%s,%s,%s,%s,%s,%s" % (first_line.split(',')[0],\
        first_line.split(',')[1].replace('\n','') ,\
        first_line.split(',')[2].replace('\n',''),\
        first_line.split(',')[3].replace('\n','') ,\
        json['latitude'],json['longitude'],json['country_code'])
      filed.write(line+'\n')
      print("%s %s"%(info,"Ok"))
    except KeyError as k:
      print("%s %s"%(err, k))
    except:
      print("%s %s"%(err, "Bad json response"))
      pass
    time.sleep(0.5)
    first_line = f.readline()

if filed:
  filed.close()
if f:
  f.close()

sys.exit(0)

